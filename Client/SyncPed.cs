﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GTA;
using GTA.Math;
using GTA.Native;
using NativeUI;
using Font = GTA.Font;

namespace GTACoOp
{
    public enum TrafficMode
    {
        None,
        Parked,
        All,
    }

    public class SyncPed
    {
        public long Host;
        public MaxMind.GeoIP2.Responses.CountryResponse geoIP;
        public Ped Character;
        public Vector3 Position;
        public Quaternion Rotation;
        public bool IsInVehicle;
        public bool IsJumping;
        public int ModelHash;
        public int CurrentWeapon;
        public bool IsShooting;
        public bool IsAiming;
        public Vector3 AimCoords;
        public float Latency;
        public bool IsHornPressed;
        public bool LightsOn;
        public bool highbeamsOn;
        public bool IsInBurnout;
        private bool _lastBurnout;
        public Vehicle MainVehicle { get; set; }

        public int VehicleSeat;
        public int PedHealth;
        public bool IsVehDead;

        public float VehicleHealth;
        public int VehicleHash;
        public Quaternion _vehicleRotation;
        public int VehiclePrimaryColor;
        public int VehicleSecondaryColor;
        public string Name;
        public bool Siren;
        public bool IsEngineRunning;
        public float VehicleRPM;

        public float Steering;
        public float WheelSpeed;
        public string Plate;
        public int RadioStation;

        public static bool Debug;

        private DateTime _stopTime;
        public float Speed
        {
            get { return _speed; }
            set
            {
                _lastSpeed = _speed;
                _speed = value;
            }
        }

        public bool IsParachuteOpen;

        public double AverageLatency
        {
            get { return _latencyAverager.Count == 0 ? 0 : _latencyAverager.Average(); }
        }

        public DateTime LastUpdateReceived
        {
            get { return _lastUpdateReceived; }
            set
            {
                if (_lastUpdateReceived != null)
                {
                    _latencyAverager.Enqueue(value.Subtract(_lastUpdateReceived).TotalMilliseconds);
                    if (_latencyAverager.Count >= 10)
                        _latencyAverager.Dequeue();
                }

                _lastUpdateReceived = value;
            }
        }

        public Dictionary<int, int> VehicleMods
        {
            get { return _vehicleMods; }
            set
            {
                if (value == null) return;
                _vehicleMods = value;
            }
        }

        public Dictionary<int, int> PedProps
        {
            get { return _pedProps; }
            set
            {
                if (value == null) return;
                _pedProps = value;
            }
        }

        private Vector3 _lastVehiclePos;
        private Vector3 _carPosOnUpdate;
        public Vector3 VehiclePosition
        {
            get { return _vehiclePosition; }
            set
            {
                _lastVehiclePos = _vehiclePosition;
                _vehiclePosition = value;

                if (MainVehicle != null)
                    _carPosOnUpdate = MainVehicle.Position;
            }
        }

        private Vector3 _lastVehVel;
        public Vector3 VehicleVelocity
        {
            get { return _vehicleVelocity; }
            set
            {
                _lastVehVel = _vehicleVelocity;
                _vehicleVelocity = value;
            }
        }

        private Quaternion? _lastVehicleRotation;
        public Quaternion VehicleRotation
        {
            get { return _vehicleRotation; }
            set
            {
                _lastVehicleRotation = _vehicleRotation;
                _vehicleRotation = value;
            }
        }

        private bool _lastVehicle;
        private uint _switch;
        private bool _lastAiming;
        private float _lastSpeed;
        private bool _lastShooting;
        private bool _lastJumping;
        private bool _blip;
        private bool _justEnteredVeh;
        private DateTime _lastHornPress = DateTime.Now;
        private int _relGroup;
        private DateTime _enterVehicleStarted;
        private Vector3 _vehiclePosition;
        private Dictionary<int, int> _vehicleMods;
        private Dictionary<int, int> _pedProps;

        private Queue<double> _latencyAverager;

        private bool _isStreamedIn;
        private Blip _mainBlip;
        private bool _lastHorn;
        private Prop _parachuteProp;

        public SyncPed(int hash, Vector3 pos, Quaternion rot, bool blip = true)
        {
            Position = pos;
            Rotation = rot;
            ModelHash = hash;
            _blip = blip;

            _latencyAverager = new Queue<double>();

            _relGroup = World.AddRelationshipGroup("SYNCPED");
            World.SetRelationshipBetweenGroups(Relationship.Neutral, _relGroup, Game.Player.Character.RelationshipGroup);
            World.SetRelationshipBetweenGroups(Relationship.Neutral, Game.Player.Character.RelationshipGroup, _relGroup);
        }

        public void SetBlipName(Blip blip, string text)
        {
            Function.Call(Hash._0xF9113A30DE5C6670, "STRING");
            Function.Call(Hash._ADD_TEXT_COMPONENT_STRING, text);
            Function.Call(Hash._0xBC38B49BCB83BC9B, blip);
        }

        private int _modSwitch = 0;
        private int _clothSwitch = 0;
        private DateTime _lastUpdateReceived;
        private float _speed;
        private Vector3 _vehicleVelocity;

        public void DisplayLocally()
        {
            try {
                float hRange = 250f;
                var gPos = IsInVehicle ? VehiclePosition : Position;
                var inRange = Game.Player.Character.IsInRangeOf(gPos, hRange);
            
                if (inRange && !_isStreamedIn)
                {
                    _isStreamedIn = true;
                    if (_mainBlip != null)
                    {
                        _mainBlip.Remove();
                        _mainBlip = null;
                    }
                }
                else if(!inRange && _isStreamedIn)
                {
                    Clear();
                    _isStreamedIn = false;
                }

                if (!inRange)
                {
                    if (_mainBlip == null && _blip)
                    {
                        _mainBlip = World.CreateBlip(gPos);
                        _mainBlip.Color = BlipColor.White;
                        _mainBlip.Scale = 0.8f;
                        SetBlipName(_mainBlip, Name == null ? "<nameless>" : Name);
                    }
                    if(_blip && _mainBlip != null)
                        _mainBlip.Position = gPos;
                    return;
                }

                if (Character == null || !Character.Exists() || (!Character.IsInRangeOf(gPos, hRange) && DateTime.Now.Subtract(LastUpdateReceived).TotalSeconds < 5) || Character.Model.Hash != ModelHash || (Character.IsDead && PedHealth > 0))
                {
                    if (Character != null) Character.Delete();

                    Character = World.CreatePed(new Model(ModelHash), gPos, Rotation.Z);
                    if (Character == null) return;

                    Character.BlockPermanentEvents = true;
                    Character.IsInvincible = true;
                    Character.CanRagdoll = false;
                    Character.RelationshipGroup = _relGroup;
                    if (_blip)
                    {
                        Character.AddBlip();
                        if (Character.CurrentBlip == null) return;
                        Character.CurrentBlip.Color = BlipColor.White;
                        Character.CurrentBlip.Scale = 0.8f;
                        SetBlipName(Character.CurrentBlip, Name);
                    }
                    return;
                }

                if (!Character.IsOccluded && Character.IsInRangeOf(Game.Player.Character.Position, 20f))
                {
                    var targetPos = Character.GetBoneCoord(Bone.IK_Head) + new Vector3(0, 0, 0.5f);

                    targetPos += Character.Velocity / Game.FPS;

                    Function.Call(Hash.SET_DRAW_ORIGIN, targetPos.X, targetPos.Y, targetPos.Z, 0);

                    var nameText = Name ?? "<nameless>";

                    var dist = (GameplayCamera.Position - Character.Position).Length();
                    var sizeOffset = Math.Max(1f - (dist / 30f), 0.3f);

                    new UIResText(nameText, new Point(0, 0), 0.4f * sizeOffset, Color.WhiteSmoke, Font.ChaletLondon, UIResText.Alignment.Centered)
                    {
                        Outline = true,
                    }.Draw();

                    Function.Call(Hash.CLEAR_DRAW_ORIGIN);
                }

                if ((!_lastVehicle && IsInVehicle && VehicleHash != 0) || (_lastVehicle && IsInVehicle && (MainVehicle == null || !Character.IsInVehicle(MainVehicle) || MainVehicle.Model.Hash != VehicleHash || VehicleSeat != Util.GetPedSeat(Character))))
                {
                    if (MainVehicle != null && Util.IsVehicleEmpty(MainVehicle))
                        MainVehicle.Delete();

                    var vehs = World.GetAllVehicles().OrderBy(v =>
                    {
                        if (v == null) return float.MaxValue;
                        return (v.Position - Character.Position).Length();
                    }).ToList();


                    if (vehs.Any() && vehs[0].Model.Hash == VehicleHash && vehs[0].IsInRangeOf(gPos, 3f))
                    {
                        if (Debug)
                        {
                            if (MainVehicle != null) MainVehicle.Delete();
                            MainVehicle = World.CreateVehicle(new Model(VehicleHash), VehiclePosition, VehicleRotation.Z);
                        }
                        else
                            MainVehicle = vehs[0];

                        if (Game.Player.Character.IsInVehicle(MainVehicle) &&
                            VehicleSeat == Util.GetPedSeat(Game.Player.Character))
                        {
                            Game.Player.Character.Task.WarpOutOfVehicle(MainVehicle);
                            UI.Notify("~r~Car jacked!");
                        }
                    }
                    else
                    {
                        MainVehicle = World.CreateVehicle(new Model(VehicleHash), gPos, 0);
                    }

                    if (MainVehicle != null)
                    {
                        if (VehicleSeat == -1)
                            MainVehicle.Position = VehiclePosition;
                        MainVehicle.PrimaryColor = (VehicleColor)VehiclePrimaryColor;
                        MainVehicle.SecondaryColor = (VehicleColor)VehicleSecondaryColor;
                        MainVehicle.IsInvincible = true;
                        Character.Task.WarpIntoVehicle(MainVehicle, (VehicleSeat)VehicleSeat);

                        /*if (_playerSeat != -2 && !Game.Player.Character.IsInVehicle(_mainVehicle))
                        { // TODO: Fix me.
                            Game.Player.Character.Task.WarpIntoVehicle(_mainVehicle, (VehicleSeat)_playerSeat);
                        }*/
                    }

                    _lastVehicle = true;
                    _justEnteredVeh = true;
                    _enterVehicleStarted = DateTime.Now;
                    return;
            }
           
            if (_lastVehicle && _justEnteredVeh && IsInVehicle && !Character.IsInVehicle(MainVehicle) && DateTime.Now.Subtract(_enterVehicleStarted).TotalSeconds <= 4)
            {
                return;
            }
            _justEnteredVeh = false;

            if (_lastVehicle && !IsInVehicle && MainVehicle != null)
            {
                if (Character != null) Character.Task.LeaveVehicle(MainVehicle, true);
            }

            if (Character != null)
            {
                Character.Health = (int)((PedHealth / (float)100) * Character.MaxHealth);
            }

            _switch++;

            if (!inRange)
            {
                if (Character != null && DateTime.Now.Subtract(LastUpdateReceived).TotalSeconds < 10)
                {
                    if (!IsInVehicle)
                    {
                        Character.PositionNoOffset = gPos;
                    }
                    else if (MainVehicle != null && GetResponsiblePed(MainVehicle).Handle == Character.Handle)
                    {
                        MainVehicle.Position = VehiclePosition;
                        MainVehicle.Quaternion = VehicleRotation;
                    }
                }
                return;
            }

            if (IsInVehicle)
            {
                if (VehicleSeat == (int) GTA.VehicleSeat.Driver ||
                    MainVehicle.GetPedOnSeat(GTA.VehicleSeat.Driver) == null)
                {
                    MainVehicle.EngineHealth = VehicleHealth;
                    if (IsVehDead && !MainVehicle.IsDead)
                    {
                        MainVehicle.IsInvincible = false;
                        MainVehicle.Explode();
                    }
                    else if (!IsVehDead && MainVehicle.IsDead)
                    {
                        MainVehicle.IsInvincible = true;
                        if (MainVehicle.IsDead)
                            MainVehicle.Repair();
                    }

                    MainVehicle.PrimaryColor = (VehicleColor) VehiclePrimaryColor;
                    MainVehicle.SecondaryColor = (VehicleColor) VehicleSecondaryColor;

                    if (MainVehicle.EngineRunning != IsEngineRunning)
                        MainVehicle.EngineRunning = IsEngineRunning;

                    if (Plate != null || Plate != null && MainVehicle.NumberPlate != Plate)
                        MainVehicle.NumberPlate = Plate;

                    var radioStations = Util.GetRadioStations();

                    if (radioStations?.ElementAtOrDefault(RadioStation) != null)
                    {
                        Function.Call(Hash.SET_VEH_RADIO_STATION, radioStations[RadioStation]);
                    }

                    if (VehicleMods != null && _modSwitch%50 == 0 &&
                        Game.Player.Character.IsInRangeOf(VehiclePosition, 30f))
                    {
                        var id = _modSwitch/50;

                        if (VehicleMods.ContainsKey(id) && VehicleMods[id] != MainVehicle.GetMod((VehicleMod) id))
                        {
                            Function.Call(Hash.SET_VEHICLE_MOD_KIT, MainVehicle.Handle, 0);
                            MainVehicle.SetMod((VehicleMod) id, VehicleMods[id], false);
                            Function.Call(Hash.RELEASE_PRELOAD_MODS, id);
                        }
                    }
                    _modSwitch++;

                    if (_modSwitch >= 2500)
                        _modSwitch = 0;

                    if (IsHornPressed && !_lastHorn)
                    {
                        _lastHorn = true;
                        MainVehicle.SoundHorn(99999);
                    }

                    if (!IsHornPressed && _lastHorn)
                    {
                        _lastHorn = false;
                        MainVehicle.SoundHorn(1);
                    }

                    if (IsInBurnout && !_lastBurnout)
                    {
                        Function.Call(Hash.SET_VEHICLE_BURNOUT, MainVehicle, true);
                        Function.Call(Hash.TASK_VEHICLE_TEMP_ACTION, Character, MainVehicle, 23, 120000); // 30 - burnout
                    }
                    else if (!IsInBurnout && _lastBurnout)
                    {
                        Function.Call(Hash.SET_VEHICLE_BURNOUT, MainVehicle, false);
                        Character.Task.ClearAll();
                    }

                    _lastBurnout = IsInBurnout;

                    Function.Call(Hash.SET_VEHICLE_BRAKE_LIGHTS, MainVehicle, Speed > 0.2 && _lastSpeed > Speed);

                    if (MainVehicle.LightsOn != LightsOn)
                        MainVehicle.LightsOn = LightsOn;

                    if (MainVehicle.HighBeamsOn != highbeamsOn)
                        MainVehicle.HighBeamsOn = highbeamsOn;

                    if (MainVehicle.SirenActive && !Siren)
                        MainVehicle.SirenActive = Siren;
                    else if (!MainVehicle.SirenActive && Siren)
                        MainVehicle.SirenActive = Siren;

                    MainVehicle.CurrentRPM = VehicleRPM; //no vehicle noise fixed
                    MainVehicle.SteeringAngle = (float)(Math.PI / 180) * Steering;

                        if (Speed > 0.2f || IsInBurnout)
                        {
                            float alpha = Util.Unlerp(currentInterop.StartTime, Environment.TickCount, currentInterop.FinishTime);

                            alpha = Util.Clamp(0f, alpha, 1.5f);

                            currentInterop.LastAlpha = alpha;

                            Vector3 comp = Util.Lerp(new Vector3(), alpha - currentInterop.LastAlpha, currentInterop.vecError);

                            if (alpha == 1.5f)
                            {
                                currentInterop.FinishTime = 0;
                            }

                            MainVehicle.Velocity = VehicleVelocity + ((VehiclePosition + comp) - MainVehicle.Position);

                            _stopTime = DateTime.Now;
                            _carPosOnUpdate = MainVehicle.Position;
                        }
                        else if (DateTime.Now.Subtract(_stopTime).TotalMilliseconds <= 1000)
                        {
                            MainVehicle.PositionNoOffset = Util.LinearVectorLerp(_carPosOnUpdate, VehiclePosition + (VehiclePosition - _lastVehiclePos),
                                (int)DateTime.Now.Subtract(_stopTime).TotalMilliseconds, 1000);
                        }
                        else
                        {
                            MainVehicle.PositionNoOffset = VehiclePosition;
                        }

                        if (_lastVehicleRotation != null)
                        {
                            MainVehicle.Quaternion = GTA.Math.Quaternion.Slerp(_lastVehicleRotation.Value,
                                _vehicleRotation,
                                Math.Min(1.5f, (float)DateTime.Now.Subtract(LastUpdateReceived).TotalSeconds / (float)AverageLatency));
                        }
                        else
                        {
                            MainVehicle.Quaternion = _vehicleRotation;
                        }
                    }
                }
                else
                {
                    if (PedProps != null && _clothSwitch%50 == 0 && Game.Player.Character.IsInRangeOf(Position, 30f))
                    {
                        var id = _clothSwitch/50;

                        if (PedProps.ContainsKey(id) &&
                            PedProps[id] != Function.Call<int>(Hash.GET_PED_DRAWABLE_VARIATION, Character.Handle, id))
                        {
                            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Character.Handle, id, PedProps[id], 0, 0);
                        }
                    }

                    _clothSwitch++;
                    if (_clothSwitch >= 750)
                        _clothSwitch = 0;

                    if (Character.Weapons.Current.Hash != (WeaponHash) CurrentWeapon)
                    {
                        var wep = Character.Weapons.Give((WeaponHash) CurrentWeapon, 9999, true, true);
                        Character.Weapons.Select(wep);
                    }

                    if (!_lastJumping && IsJumping)
                    {
                        Character.Task.Jump();
                    }

                    if (IsParachuteOpen)
                    {
                        if (_parachuteProp == null)
                        {
                            _parachuteProp = World.CreateProp(new Model(1740193300), Character.Position,
                                Character.Rotation, false, false);
                            _parachuteProp.FreezePosition = true;
                            Function.Call(Hash.SET_ENTITY_COLLISION, _parachuteProp.Handle, false, 0);
                        }
                        Character.FreezePosition = true;
                        Character.Position = Position - new Vector3(0, 0, 1);
                        Character.Quaternion = Rotation;
                        _parachuteProp.Position = Character.Position + new Vector3(0, 0, 3.7f);
                        _parachuteProp.Quaternion = Character.Quaternion;

                        Character.Task.PlayAnimation("skydive@parachute@first_person", "chute_idle_right", 8f, 5000,
                            false, 8f);
                    }
                    else
                    {
                        var dest = Position;
                        Character.FreezePosition = false;

                        if (_parachuteProp != null)
                        {
                            _parachuteProp.Delete();
                            _parachuteProp = null;
                        }

                        const int threshold = 50;
                        if (IsAiming && !IsShooting && !Character.IsInRangeOf(Position, 0.5f) && _switch%threshold == 0)
                        {
                            Function.Call(Hash.TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD, Character.Handle, dest.X, dest.Y,
                                dest.Z, AimCoords.X, AimCoords.Y, AimCoords.Z, 2f, 0, 0x3F000000, 0x40800000, 1, 512, 0,
                                (uint) FiringPattern.FullAuto);
                        }
                        else if (IsAiming && !IsShooting && Character.IsInRangeOf(Position, 0.5f))
                        {
                            Character.Task.AimAt(AimCoords, 100);
                        }

                        if (!Character.IsInRangeOf(Position, 0.5f) &&
                            ((IsShooting && !_lastShooting) ||
                             (IsShooting && _lastShooting && _switch%(threshold*2) == 0)))
                        {
                            Function.Call(Hash.TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD, Character.Handle, dest.X, dest.Y,
                                dest.Z, AimCoords.X, AimCoords.Y, AimCoords.Z, 2f, 1, 0x3F000000, 0x40800000, 1, 0, 0,
                                (uint) FiringPattern.FullAuto);
                        }
                        else if ((IsShooting && !_lastShooting) ||
                                 (IsShooting && _lastShooting && _switch%(threshold/2) == 0))
                        {
                            Function.Call(Hash.TASK_SHOOT_AT_COORD, Character.Handle, AimCoords.X, AimCoords.Y,
                                AimCoords.Z, 1500, (uint) FiringPattern.FullAuto);
                        }

                        if (!IsAiming && !IsShooting && !IsJumping)
                        {
                            if (!Character.IsInRangeOf(Position, 0.5f))
                            {
                                Character.Task.RunTo(Position, true, 500);
                                //var targetAngle = Rotation.Z/Math.Sqrt(1 - Rotation.W*Rotation.W);
                                //Function.Call(Hash.TASK_GO_STRAIGHT_TO_COORD, Character.Handle, Position.X, Position.Y, Position.Z, 5f, 3000, targetAngle, 0);
                            }
                            if (!Character.IsInRangeOf(Position, 5f))
                            {
                                Character.Position = dest - new Vector3(0, 0, 1f);
                                Character.Quaternion = Rotation;
                            }
                        }
                    }
                    _lastJumping = IsJumping;
                    _lastShooting = IsShooting;
                    _lastAiming = IsAiming;
                }
                _lastVehicle = IsInVehicle;
            }
            catch (Exception ex)
            {
                Sentry.Capture(ex);

                UI.Notify("Sync error: "+ex.Message);
            }
        }

        struct interpolation
        {
            public Vector3 vecStart;
            public Vector3 vecTarget;
            public Vector3 vecError;
            public int StartTime;
            public int FinishTime;
            public float LastAlpha;
        }

        private interpolation currentInterop = new interpolation();

        public void StartInterpolation()
        {
            currentInterop = new interpolation();

            currentInterop.vecTarget = VehiclePosition;
            currentInterop.vecError = VehiclePosition - _lastVehiclePos;
            currentInterop.vecError *= Util.Lerp(0.25f, Util.Unlerp(100, 100, 400), 1f);
            currentInterop.StartTime = Environment.TickCount;
            currentInterop.FinishTime = Environment.TickCount + 100;
            currentInterop.LastAlpha = 0f;
        }

        public static Ped GetResponsiblePed(Vehicle veh)
        {
            if (veh == null || veh.Handle == 0 || !veh.Exists()) return new Ped(0);

            if (veh.GetPedOnSeat(GTA.VehicleSeat.Driver).Handle != 0) return veh.GetPedOnSeat(GTA.VehicleSeat.Driver);

            for (int i = 0; i < veh.PassengerSeats; i++)
            {
                if (veh.GetPedOnSeat((VehicleSeat)i).Handle != 0) return veh.GetPedOnSeat((VehicleSeat)i);
            }

            return new Ped(0);
        }

        public void Clear()
        {
            try
            {
                if (Character != null)
                {
                    Character.Model.MarkAsNoLongerNeeded();
                    Character.Delete();
                }
                if (_mainBlip != null)
                {
                    _mainBlip.Remove();
                    _mainBlip = null;
                }
                if (MainVehicle != null && Util.IsVehicleEmpty(MainVehicle))
                {
                    MainVehicle.Model.MarkAsNoLongerNeeded();
                    MainVehicle.Delete();
                }
                if (_parachuteProp != null)
                {
                    _parachuteProp.Delete();
                    _parachuteProp = null;
                }
            } catch (Exception ex)
            {
                UI.Notify("Clear sync error: " + ex.Message);
            }
        }
    }
}